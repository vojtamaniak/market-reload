<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="fe_accounts")
 */
class FeEconomyAccount
{

    //use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="string")
     * @ORM\Id
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $uuid;

    /**
     * @ORM\Column(type="float")
     */
    protected $money;
}