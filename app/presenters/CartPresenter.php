<?php
namespace App\Presenters;

use App\CartItem;
use App\Coupon;
use App\FeEconomyAccount;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class CartPresenter extends BasePresenter
{

    public function startup(){
        parent::startup();
        if(!$this->getUser()->isLoggedIn()){
            $this->flashMessage($this->translator->translate("messages.basic.notLoggedIn"));
            $this->redirect("Sign:in");
        }
    }

    /**
     * @var \Websend @inject
     */
    public $websend;

    public function renderDefault(){
        $items = $this->entMng->getRepository(CartItem::class)->findBy(array("user" => $this->getUser()->getId()));
        $this->template->items = $items;
        $this->template->price = $this->calculatePrice($items);
        $this->template->priceNoDiscount = $this->calculatePriceWithoutDiscount($items);
        $this->template->coupon = $this->entMng->getRepository(Coupon::class)->findOneBy(["token" => $this->getSession("coupons")->coupon]);
    }

    protected function createComponentCountForm()
    {
        $form = new Form;

        $form->addText("count")->addRule(Form::INTEGER, $this->translator->translate("messages.cart.countNumeric"))->setAttribute("style", "width:50px; display:inline; height:auto; padding:0; text-align: center;")->setAttribute('class', 'form-control')->setType("number");
        $form->addHidden("itemid");

        $form->addSubmit("submit", $this->translator->translate("messages.carttable.changeCountButton"))->setAttribute("style", "font-size:12px; margin-left:5px")->setAttribute('class', 'btn btn-xs btn-primary');

        $form->onSuccess[] = $this->countFormSucceeded;
        return $form;
    }

    public function countFormSucceeded(Form $form, ArrayHash $values){
        $item = $this->entMng->getRepository(CartItem::class)->find($values->itemid);
        $name = $item->item->name;

        if($item == null){
            $this->flashMessage($this->translator->translate("messages.cart.noItemMatched"), "danger");
            $this->redirect("this");
        }

        if($item->user->id !== $this->getUser()->getId()){
            $this->flashMessage($this->translator->translate("messages.cart.notOwner"), "danger");
            $this->redirect("this");
        }

        if($values->count < 1){
            $this->entMng->remove($item);

            if(!$this->isAjax()){
                $this->flashMessage($this->translator->translate("messages.cart.itemRemoved"));
                $this->redirect("this");
            }

            $this->payload->successMessage = $this->translator->translate("messages.cart.itemRemoved");
            $this->redrawControl("cartContent");
        }else{
            $item->count = $values->count;

            if(!$this->isAjax()){
                $this->flashMessage($this->translator->translate("messages.cart.countChanged", ["name" => $name, "count" => $values->count]));
                $this->redirect("this");
            }

            $this->payload->successMessage = $this->translator->translate("messages.cart.countChanged", ["name" => $name, "count" => $values->count]);
            $this->redrawControl("cartContent");
        }
        $this->entMng->flush();
    }

    private function calculatePrice($items){
        $price = 0;

        $token = $this->getSession("coupons")->coupon;
        if($token === null){
            foreach ($items as $item){
                $price += ($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count;
            }
        }
        else {
            $coupon = $this->entMng->getRepository(Coupon::class)->findOneBy(["token" => $token]);
            switch ($coupon->type){
                case 0:
                    foreach ($items as $item){
                        $price += ($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count;
                    }

                    if($coupon->discount > $price){
                        $price = 0;
                        $this->flashMessage($this->translator->translate("messages.coupon.discountHigherThanPrice"), "warning");
                    } else {
                        $price -= $coupon->discount;
                    }
                    break;
                case 1:
                    foreach ($items as $item){
                        $price += ($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count;
                    }

                    $price -= $price * $coupon->discount / 100;
                    break;
                case 2:
                    foreach ($items as $item){
                        if($coupon->item->id == $item->item->id){
                            $price1 = ($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count;
                            $price += $coupon->discount > $price1 ? 0 : $price1 - $coupon->discount;
                        }else{
                            $price += ($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count;
                        }
                    }
                    break;
                case 3:
                    $used = false;
                    foreach ($items as $item){

                        if($coupon->item->id == $item->item->id){
                            $pricePerThisOne = ($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count;
                            $price += $pricePerThisOne - ($pricePerThisOne * $coupon->discount / 100);
                            $used = true;
                        }else{
                            $price += ($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count;
                        }
                    }

                    if(!$used){
                        $this->getSession("coupons")->coupon = null;

                        $this->flashMessage($this->translator->translate("messages.coupon.itemNoLongerInCart", ["item" => $coupon->item->name]), "warning");
                        $this->redirect("this");
                    }
                    break;
                default:
                    $this->getSession("coupons")->coupon = null;
                    throw new \Exception("Unknown or unimplemented coupon type");
            }
        }


        return $price;
    }

    private function calculatePriceWithoutDiscount($items){
        $price = 0;

        foreach ($items as $item){
            $price += ($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count;
        }

        return $price;
    }

    protected function createComponentCouponForm(){
        $form = new Form;
        
        $form->addText("coupon")->setRequired($this->translator->translate("message.cart.couponFormRequired"));
        $form->addSubmit("submit", $this->translator->translate("messages.cart.useCouponButton"));

        $form->onSuccess[] = $this->couponFormSucceeded;
        return $form;
    }

    public function couponFormSucceeded(Form $form, ArrayHash $values){
        $coupon = $this->entMng->getRepository(Coupon::class)->findOneBy(["token" => $values->coupon]);

        if($coupon === null){
            $this->flashMessage($this->translator->translate("messages.coupon.invalid"), "danger");
            $this->redirect("this");
        }

        if($coupon->used == true){
            $this->flashMessage($this->translator->translate("messages.coupon.alreadyUsed"));
            $this->redirect("this");
        }

        $session = $this->getSession("coupons");
        $session->setExpiration("5 hour");

        switch($coupon->type){
            case 0:
            case 1:
                $session->coupon = $coupon->token;
                break;
            case 2:
            case 3:
                if($this->entMng->getRepository(CartItem::class)->findOneBy(["user" => $this->getUser()->getId(), "item" => $coupon->item->id]) === null){
                    $this->flashMessage($this->translator->translate("messages.coupon.itemNotInCart"), "danger");
                    $this->redirect("this");
                }

                $session->coupon = $coupon->token;
            break;
        }

        $this->flashMessage($this->translator->translate("messages.coupon.couponUsed"), "info");
        $this->redirect("this");
    }

    public function handleClearCart(){
        $items = $this->entMng->getRepository(CartItem::class)->findBy(["user" => $this->getUser()->getId()]);

        $this->entMng->remove($items);
        $this->entMng->flush();

        $this->flashMessage($this->translator->translate("messages.cart.emptied"), "success");

        if(!$this->isAjax()){
            $this->redirect("this");
        }

        $this->redrawControl("cartContent");
    }

    public function handleRemoveFromCart($itemId){
        $item = $this->entMng->getRepository(CartItem::class)->find($itemId);

        if($item->user->id !== $this->getUser()->getId()){
            $this->flashMessage($this->translator->translate("messages.cart.notOwner"), "danger");
            $this->redirect("this");
        }

        $name = $item->item->name;
        $this->entMng->remove($item);
        $this->entMng->flush();

        if(!$this->isAjax()){
            $this->flashMessage($this->translator->translate("messages.cart.itemRemoved", ["name" => $name]), "success");
            $this->redirect("this");
        }

        $this->payload->successMessage = $this->translator->translate("messages.cart.itemRemoved", ["name" => $name]);
        $this->redrawControl("cartContent");
    }

    public function handleOrder(){
        $items = $this->entMng->getRepository(CartItem::class)->findBy(["user" => $this->getUser()->getId()]);
        $username = $this->getUser()->getIdentity()->username;

        if(count($items) == 0) {
            $this->flashMessage($this->translator->translate("messages.buy.cartEmpty"), "warning");
            $this->redirect("this");
        }

        $coupon = $this->entMng->getRepository(Coupon::class)->findOneBy(["token" => $this->getSession("coupons")->coupon]);
        if($coupon !== null && $coupon->used){
            $this->getSession("coupons")->coupon = null;
            $this->flashMessage($this->translator->translate("messages.coupon.alreadyUsed"), "warning");
            $this->redirect("this");
        }

        if(!$this->websend->connect()){
            $this->flashMessage($this->translator->translate("messages.buy.serverOffline"), "danger");
            $this->redirect("this");
        }
        if(!$this->websend->writeOutputToPlayer("", $username)){
            $this->flashMessage($this->translator->translate("messages.buy.playerOffline"));
            $this->redirect("this");
        }

        $price = $this->calculatePrice($items);
        $balance = 0;
        $moneyCmd = "";
        switch($this->par->getEconomyPlugin()){
            case 0:
                $balance = $this->entMng->getRepository(FeEconomyAccount::class)->findOneBy(["name" => $username])->money;
                $moneyCmd = "fe deduct ". $username. " ". $price;
                break;
            default:
                throw new \Exception("Economy plugin error.");
        }

        if($price > $balance){
            $this->flashMessage($this->translator->translate("messages.buy.notEnoughMoney"));
            $this->redirect("this");
        }

        $this->websend->doCommandAsConsole($moneyCmd);
        foreach ($items as $item){
            if($item->count > 64){
                $remainingCount = $item->count;
                while($remainingCount > 64){
                    $this->websend->doCommandAsConsole("minecraft:give ". $username ." ". $item->item->newid ." 64");
                    $remainingCount -= 64;
                }
                $this->websend->doCommandAsConsole("minecraft:give ". $username ." ". $item->item->newid ." ". $remainingCount);
            }else{
                $this->websend->doCommandAsConsole("minecraft:give ". $username ." ". $item->item->newid ." ". $item->count);
            }
        }
        
        $this->websend->writeOutputToPlayer($this->translator->translate("messages.buy.serverSuccess", ["price" => $price, "balance" => $balance]), $username);

        if($coupon !== null){
            $coupon->used = true;
            $this->getSession("coupons")->coupon = null;
        }

        $this->entMng->remove($items);
        $this->entMng->flush();
        
        $this->flashMessage($this->translator->translate("messages.buy.success", ["price" => $price, "balance" => $balance]), "success");
        $this->redirect("Homepage:");
    }

    public function handleRemoveCoupon(){
        $this->getSession("coupons")->coupon = null;

        $this->flashMessage("messages.coupon.noLongerUse", "info");
        $this->redirect("this");
    }
}