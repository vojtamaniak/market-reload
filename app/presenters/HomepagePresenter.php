<?php

namespace App\Presenters;

use App\CartItem;
use App\FeEconomyAccount;
use App\Item;
use App\User;
use Nette;
use Tracy\Debugger;


class HomepagePresenter extends BasePresenter
{

    /**
     * @var Nette\Utils\Paginator
     */
    private $paginator;

    /**
     * @var \Websend @inject
     */
    public $websend;

    private $items;

    public function actionDefault($page = 1)
    {
        $paginator = new Nette\Utils\Paginator;

        $paginator->setItemCount($this->entMng->getRepository(Item::class)->countBy(["visible" => "1"]));
        $paginator->setItemsPerPage(24);
        $paginator->setPage($page);
        $length = $paginator->getLength();

        if ($page < 1 || $page > $paginator->getLastPage()) {
            $this->flashMessage($this->translator->translate("messages.paginator.limit", ["maxPage" => $paginator->getLastPage()]));
            $paginator->setPage(1);
        }
        $this->paginator = $paginator;
    }

    public function renderDefault(){
        $this->template->items = $this->items === null ? $this->entMng->getRepository(Item::class)->findBy(["visible" => 1], [], $this->paginator->getLength(), $this->paginator->getOffset()) : $this->items;

        $this->template->paginator = $this->paginator;
    }

    public function handleAddToCart($itemId, $itemCount){
        if(!$this->getUser()->isLoggedIn()){
            $this->flashMessage($this->translator->translate("messages.basic.notLoggedIn"));
            $this->redirect("this");
        }

        if((!is_numeric($itemCount)) || (!is_numeric($itemId))){
            Debugger::log("handleAddToCart -> itemcount or itemid is not numeric", "warning");
            $this->redirect("this");
        }

        if($itemCount < 1){
            $this->flashMessage($this->translator->translate("messages.cart.minimalCount"));
            $this->redirect("this");
        }

        $item = $this->entMng->getRepository(Item::class)->findOneBy(["id" => $itemId]);

        if($item == null){
            $this->flashMessage($this->translator->translate("messages.cart.noItemMatched"));
            $this->redirect("this");
        }
        
        $cartItem = $this->entMng->getRepository(CartItem::class)->findOneBy(["item" => $itemId, "user" => $this->getUser()->getId()]);

        if($cartItem == null) {
            $cartItem = new CartItem;

            $cartItem->user = $this->entMng->getRepository(User::class)->find($this->getUser()->getId());
            $cartItem->item = $item;
            $cartItem->count = $itemCount;
            $this->entMng->persist($cartItem);
        }else{
            $cartItem->count += $itemCount;
        }

        $this->entMng->flush();

        if(!$this->isAjax()){
            $this->redirect("this");
        }
        $this->payload->successMessage = $this->translator->translate("messages.cart.itemAdded", ["count" => $itemCount, "itemName" => $item->name]);
        $this->redrawControl("totalCost");
    }

    public function handleFastToServer($itemId, $itemCount){
        if(!$this->getUser()->isLoggedIn()){
            $this->flashMessage($this->translator->translate("messages.basic.notLoggedIn"));
            $this->redirect("this");
        }

        $username = $this->getUser()->getIdentity()->username;
        if((!is_numeric($itemCount)) || (!is_numeric($itemId))){
            Debugger::log("handleAddToCart -> itemcount or itemid is not numeric", "warning");
            $this->redirect("this");
        }

        if($itemCount < 1){
            $this->flashMessage($this->translator->translate("messages.cart.minimalCount"));
            $this->redirect("this");
        }

        $item = $this->entMng->getRepository(Item::class)->findOneBy(["id" => $itemId]);

        if($item == null){
            $this->flashMessage($this->translator->translate("messages.cart.noItemMatched"));
            $this->redirect("this");
        }

        if(!$this->websend->connect()){
            $this->payload->errorMessage = $this->translator->translate("messages.cart.serverOffline");
            return;
        }
        if(!$this->websend->writeOutputToPlayer("", $username)){
            $this->payload->errorMessage = $this->translator->translate("messages.cart.playerOffline");
            return;
        }

        $price = ($item->price - $item->price * $item->discount / 100) * $itemCount;

        $balance = 0;

        switch($this->par->getEconomyPlugin()){
            case 0:
                $balance = $this->entMng->getRepository(FeEconomyAccount::class)->findOneBy(["name" => $username])->money;
                $moneyCmd = "fe deduct ". $username. " ". $price;
                break;
            default:
                throw new \Exception("Economy plugin error - Unknown economy plugin");
        }

        if($price > $balance){
            $this->payload->errorMessage = $this->translator->translate("message.fastToServer.noMoney");
            return;
        }

        $this->websend->doCommandAsConsole($moneyCmd);

        if($itemCount > 64){
            $remainingCount = $itemCount;
            while($remainingCount > 64){
                $this->websend->doCommandAsConsole("minecraft:give ". $username ." ".$item->newid ." 64");
                $remainingCount -= 64;
            }
            $this->websend->doCommandAsConsole("minecraft:give ". $username . " ". $item->newid ." ". $remainingCount);
        }else{
            $this->websend->doCommandAsConsole("minecraft:give ". $username ." ". $item->newid ." ". $itemCount);
        }

        $this->payload->successMessage = $this->translator->translate("messages.fastToServer.success", ["itemName" => $item->name, "count" => $itemCount, "price" => $price, 'currency' => $this->settings->currency]);
        $this->redrawControl("userInfo");
    }

    public function handleSearch($q){
        if($q === ""){
            $this->items = $this->entMng->getRepository(Item::class)->findBy(["visible" => 1]);
        }else{
            $this->items = $this->entMng->getRepository(Item::class)->createQueryBuilder('i')->where('i.visible = 1')->andWhere('i.name LIKE :name')->setParameter('name', '%'.$q.'%')->getQuery()->getResult();
        }
        if(!$this->isAjax()){
            $this->redirect('this');
        }
        $this->redrawControl('items');
    }
}
