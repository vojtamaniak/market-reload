<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="ws_users")
 */
class User
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="string")
     */
    protected $username;

    /**
     * @ORM\Column(type="string")
     */
    protected $role;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $lastlogin;

    /**
     * @ORM\OneToMany(targetEntity="CartItem", mappedBy="user")
     */
    protected $cartItems;
}