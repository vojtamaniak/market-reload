<?php
namespace App;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="ws_coupons")
 */
class Coupon
{

    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="string")
     */
    protected $token;

    /**
     * @ORM\Column(type="integer")
     * 0 = $ to all, 1 = % to all, 2 = $ to item, 3 = % to item
     */
    protected $type;

    /**
     * @ORM\Column(type="integer")
     */
    protected $discount;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @var Item
     */
    protected $item;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $used;
}