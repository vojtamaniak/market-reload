<?php

namespace App\Presenters;

use App\CartItem;
use App\Params;
use App\Setting;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette;
use App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    /**
     * @var string
     * @persistent
     */
    public $locale;

    /**
     * @var Translator @inject
     */
    public $translator;

    /**
     * @var EntityManager
     * @inject
     */
    public $entMng;

    protected $settings;

    /**
     * @var Params @inject
     */
    public $par;

    public function startup()
    {
        parent::startup();
        $this->settings = $this->entMng->getRepository(Setting::class)->find(1);
    }

    public function beforeRender()
    {
        $this->template->settings = $this->settings;

        //TRODO!
        $this->template->balance = 0;
        $this->template->totalCost = 0;

        if($this->isAjax()){
            $this->redrawControl("flashMessages");
        }
	}

    public function createTemplate()
    {
        $template = parent::createTemplate();

        $template->setTranslator($this->translator);

        $template->addFilter("wsimg", function($s) {
            if (strpos($s, "http://") === FALSE && strpos($s, "https://") === FALSE) {
                $link = $this->link('Homepage:');
                $link .= "images/". $s;

                return $link;
            }else{
                return $s;
            }
        });

        $template->addFilter("couponDiscount", function ($coupon, $price){
            return $this->couponDiscount($coupon, $price);
        });

        $template->addFilter("discountReason", function ($coupon){
            switch ($coupon->type){
                case 0:
                    return $this->translator->translate("messages.coupon.discountReason", ["discount" => $coupon->discount."". $this->settings['mena']]);
                case 1:
                    return $this->translator->translate("messages.coupon.discountReason", ["discount" => $coupon->discount."%"]);
                case 2:
                    return $this->translator->translate("messages.coupon.discountReasonItem", ["discount" => $coupon->discount."".$this->settings['mena'], "item" => $coupon->item->name]);
                case 3:
                    return $this->translator->translate("messages.coupon.discountReasonItem", ["discount" => $coupon->discount."%", "item" => $coupon->item->name]);
                default:
                    return "";
            }
        });

        $template->addFilter("oneItemPrice", function ($item, $coupon){
            if($coupon !== null) {
                switch ($coupon->type) {
                    case 0:
                    case 1:
                        return ((($item->item->price - $item->item->price * $item->item->discount / 100)));
                    case 2:
                    case 3:
                        return (($item->item->price - $item->item->price * $item->item->discount / 100) - $this->couponDiscount($coupon, 0, false));
                }
            }else{
                return ((($item->item->price - $item->item->price * $item->item->discount / 100)));
            }

            
        });

        $template->addFilter("itemsPrice", function($item, $coupon){
            if($coupon !== null){
                switch ($coupon->type){
                    case 0:
                    case 1:
                        return ((($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count));
                    case 2:
                    case 3:
                        if($coupon->item->id == $item->item->id){
                            return ((($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count) - $this->couponDiscount($coupon, 0, true));
                        }
                        else{
                            return ((($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count));
                        }
                        break;
                }
            }else{
                return ((($item->item->price - $item->item->price * $item->item->discount / 100) * $item->count));
            }

        });

        return $template;
    }

    private function couponDiscount($coupon, $price, $useCount = true){
        $discount = 0;

        if($coupon === null){
            return 0;
        }
        switch($coupon->type){
            case 0:
                if($coupon->discount > $price){
                    $discount = $price;
                }else{
                    $discount = $coupon->discount;
                }
                break;
            case 1:
                $discount = $price * $coupon->discount / 100;
                break;
            case 2:
                $count = $useCount ? $this->entMng->getRepository(CartItem::class)->findOneBy(["authmeId" => $this->getUser()->getId(), "item" => $coupon->item])->count : 1;

                $price1 = ($coupon->item->price - $coupon->item->price * $coupon->item->discount / 100) * $count;
                if($coupon->discount > $price1){
                    $discount = $price1;
                }else{
                    $discount = $coupon->discount;
                }
            case 3:
                $count = $useCount ? $this->entMng->getRepository(CartItem::class)->findOneBy(["authmeId" => $this->getUser()->getId(), "item" => $coupon->item])->count : 1;

                $price1 = ($coupon->item->price - $coupon->item->price * $coupon->item->discount / 100) * $count;
                $discount = $price1 * $coupon->discount/100;
        }
        return $discount;
    }
}
