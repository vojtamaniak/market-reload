<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="ws_settings")
 */
class Setting
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="string")
     */
    protected $servername;

    /**
     * @ORM\Column(type="string")
     */
    protected $currency;
}