<?php
namespace App;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="ws_items")
 */
class Item
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $price;

    /**
     * @ORM\Column(type="integer")
     */
    protected $discount;

    /**
     * @ORM\Column(type="integer")
     */
    protected $oldid;

    /**
     * @ORM\Column(type="string")
     */
    protected $newid;

    /**
     * @ORM\Column(type="integer")
     */
    protected $itemdata;

    /**
     * @ORM\Column(type="string")
     */
    protected $image;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $visible;
    
}