<?php

namespace App\Presenters;

use App\Item;
use App\User;
use Doctrine\Common\Util\Debug;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Utils\ArrayHash;
use Nextras\Datagrid\Datagrid;
use Symfony\Component\Config\Definition\NumericNode;
use Tracy\Debugger;

class AdminPresenter extends BasePresenter
{

    public function startup()
    {
        parent::startup();

        if(!$this->getUser()->isLoggedIn()){
            $this->flashMessage($this->translator->translate("messages.basic.notLoggedIn"));
            $this->redirect("Sign:in");
        }

        if (!in_array("admin", $this->getUser()->getRoles())) {
            $this->flashMessage($this->translator->translate("messages.basic.noPermission"), "danger");
            $this->redirect("Homepage:");
        }
    }

    protected function createComponentBasicSettingsForm()
    {
        $form = new Form;

        $form->addText("serverName", $this->translator->translate("messages.administration.basicSettings.serverName"))->setRequired($this->translator->translate("messages.administration.basicSettings.serverNameEmpty"))->setValue($this->settings->servername);
        $form->addText("currency", $this->translator->translate("messages.administration.basicSettings.currency"))->setRequired($this->translator->translate("messages.administration.basicSettings.currencyEmpty"))->setValue($this->settings->currency);
        $form->addSubmit("submit", $this->translator->translate("messages.administration.basicSettings.changeButton"));

        $form->onSuccess[] = $this->basicSettingsFormSucceeded;
        return $form;
    }

    public function basicSettingsFormSucceeded(Form $form, ArrayHash $values)
    {
        $this->settings->servername = $values->serverName;
        $this->settings->currency = $values->currency;

        $this->entMng->flush();
        $this->flashMessage($this->translator->translate("messages.administration.basicSettings.saved"), "success");
        $this->redirect("this");
    }

    protected function createComponentUsersGrid()
    {
        $grid = new Datagrid;

        $grid->addColumn('id', $this->translator->translate("messages.administration.users.id"))->enableSort();
        $grid->addColumn('username', $this->translator->translate("messages.administration.users.username"))->enableSort();
        $grid->addColumn('role', $this->translator->translate("messages.administration.users.role"))->enableSort();
        $grid->addColumn('lastlogin', $this->translator->translate("messages.administration.users.lastlogin"))->enableSort();
        //$grid->addColumn('');

        $grid->setRowPrimaryKey('id');

        $grid->setDataSourceCallback(function ($filter, $order) {
            $result = $this->entMng->getRepository(User::class)->createQueryBuilder('u')->where('1=1');
            if(isset($filter['id'])){
                $result->andWhere('u.id = :i')->setParameter('i', $filter['id']);
            }
            if (isset($filter['username'])) {
                $username = is_array($filter['username']) ? $filter['username'][0] : $filter['username'];
                $result->andWhere('u.username LIKE :u')->setParameter('u', '%' . $username . '%');
            }
            if (isset($filter['role'])) {
                $result->andWhere('u.role LIKE :r')->setParameter('r', '%' . $filter['role'] . '%');
            }
            if ($order !== null) {
                $result->addOrderBy('u.' . $order[0], $order[1]);
            }
            return $result->getQuery()->getResult();
        });

        $grid->setFilterFormFactory(function () {
            $form = new Container;
            $form->addText('id')->setType('number');
            $form->addText('username');
            $form->addSelect('role', 'Role', array('admin' => $this->translator->translate("messages.administration.users.admin"), 'default' => $this->translator->translate("messages.administration.users.user")));

            $form->addSubmit('filter', $this->translator->translate("messages.administration.applyFilterButton"))->getControlPrototype()->class = 'btn btn-primary btn-sm';
            $form->addSubmit('cancel', $this->translator->translate("messages.administration.cancelFilterButton"))->getControlPrototype()->class = 'btn btn-sm';
            return $form;
        });

        $grid->addCellsTemplate(__DIR__ . "/templates/components/usersGrid.latte");

        $grid->setEditFormFactory(function ($row) {
            $form = new Container;
            $form->addSelect('role', "Role", array('admin' => $this->translator->translate("messages.administration.users.admin"), 'default' => $this->translator->translate("messages.administration.users.user")));

            $form->addSubmit('save', $this->translator->translate("messages.administration.saveRowButton"))->getControlPrototype()->class = 'btn btn-primary';
            $form->addSubmit('cancel', $this->translator->translate("messages.administration.cancelRowButton"))->getControlPrototype()->class = 'btn';

            if ($row) {
                $form->setDefaults(array('role' => $row->role));
            }

            return $form;
        });

        $grid->setEditFormCallback(function ($data) {
            $data = $data->getValues();
            $user = $this->entMng->getRepository(User::class)->find($data->id);
            $user->role = $data->role;
            $this->entMng->flush();
        });

        return $grid;
    }

    protected function createComponentItemsGrid(){
        $grid = new Datagrid;

        $grid->addColumn('id', $this->translator->translate("messages.administration.items.id"))->enableSort();
        $grid->addColumn('name', $this->translator->translate("messages.administration.items.name"))->enableSort();
        $grid->addColumn('price', $this->translator->translate("messages.administration.items.price"))->enableSort();
        $grid->addColumn('discount', $this->translator->translate("messages.administration.items.discount"))->enableSort();
        $grid->addColumn('newid', $this->translator->translate("messages.administration.items.newid"))->enableSort();
        $grid->addColumn('itemdata', $this->translator->translate("messages.administration.items.itemdata"))->enableSort();
        $grid->addColumn('image', $this->translator->translate("messages.administration.items.image"))->enableSort();
        //$grid->addColumn('visible', $this->translator->translate("messages.administration.items.visible"))->enableSort();

        $grid->setRowPrimaryKey('id');

        $grid->addCellsTemplate(__DIR__."/templates/components/itemsGrid.latte");

        $grid->setDataSourceCallback(function($filter, $order){
            $result = $this->entMng->getRepository(Item::class)->createQueryBuilder('i')->where('1=1');
            if(isset($filter['id'])){
                $result->andWhere('i.id = :id')->setParameter('id', $filter['id']);
            }
            if(isset($filter['name'])){
                $result->andWhere('i.name LIKE :name')->setParameter('name', "%".$filter['name']."%");
            }
            if(isset($filter['newid'])){
                $result->andWhere('i.newid LIKE :newid')->setParameter('newid', "%".$filter['newid']."%");
            }
            if(isset($filter['itemdata'])){
                $result->andWhere('i.itemdata = :itemdata')->setParameter('itemdata', $filter['itemdata']);
            }

            if($order !== null){
                $result->addOrderBy('i.'. $order[0], $order[1]);
            }
            return $result->getQuery()->getResult();
        });

        $grid->setFilterFormFactory(function(){
            $form = new Container;

            $form->addText('id')->setType('number');
            $form->addText('name');
            $form->addText('newid');
            $form->addText('itemdata');

            $form->addSubmit('filter', $this->translator->translate("messages.administration.applyFilterButton"))->getControlPrototype()->class = 'btn btn-primary btn-sm';
            $form->addSubmit('cancel', $this->translator->translate("messages.administration.cancelFilterButton"))->getControlPrototype()->class = 'btn btn-sm';

            return $form;
        });

        $grid->setEditFormFactory(function($def){
            $form = new Container;

            $form->addText('name');
            $form->addText('price')->setType('number')->addRule(Form::NUMERIC, $this->translator->translate("messages.administration.items.priceMustBeNumeric"));
            $form->addText('discount')->setType('number')->addRule(Form::NUMERIC, $this->translator->translate("messages.administration.items.discountMustBeNumeric"));
            $form->addText('newid');
            $form->addText('itemdata')->addRule(Form::NUMERIC, $this->translator->translate("messages.administration.items.itemdataMustBeNumeric"));
            $form->addText('image');

            $form->addSubmit('save', $this->translator->translate("messages.administration.saveRowButton"))->getControlPrototype()->class = 'btn btn-primary';
            $form->addSubmit('cancel', $this->translator->translate("messages.administration.cancelRowButton"))->getControlPrototype()->class = 'btn';

            if($def){
                $form->setDefaults(array('name' => $def->name, 'price' => $def->price, 'discount'=>$def->discount, 'newid' => $def->newid, 'itemdata' => $def->itemdata, 'image' => $def->image));
            }

            return $form;
        });

        $grid->setEditFormCallback(function($data){
            $data = $data->getValues();
            $item = $this->entMng->getRepository(Item::class)->find((int)$data->id);
            $item->name = $data->name;
            $item->price = $data->price;
            $item->discount = $data->discount;
            $item->newid = $data->newid;
            $item->itemdata = $data->itemdata;
            $item->image = $data->image;
            Debugger::fireLog($item);
            $this->entMng->flush();
        });

        return $grid;
    }

    protected function createComponentAddItemForm(){
        $form = new Form;

        $form->addText('name', $this->translator->translate('messages.administration.additem.name'))->setRequired($this->translator->translate('messages.administration.additem.nameRequired'));
        $form->addText('price', $this->translator->translate('messages.administration.additem.price'))->setRequired($this->translator->translate('messages.administration.additem.priceRequired'))->setType('number')->addRule(Form::NUMERIC, $this->translator->translate('messages.basic.notNumeric'));
        $form->addText('discount', $this->translator->translate('messages.administration.additem.discount'))->setRequired($this->translator->translate('messages.administration.additem.discountRequired'))->setType('number')->addRule(Form::NUMERIC, $this->translator->translate('messages.basic.notNumeric'))->addRule(Form::RANGE, $this->translator->translate('messages.administration.additem.discountRange', [0, 100]));
        $form->addText('newid', $this->translator->translate('messages.administration.additem.itemid'))->setRequired($this->translator->translate('messages.administration.additem.itemIdRequired'));
        //$form->addText('itemdata', $this->translator->translate('messages.administration.additem.itemdata'))->setRequired($this->translator->translate('messages.administration.additem.itemdataRequired'))->setType('number')->addRule(Form::NUMERIC, $this->translator->translate('messages.basic.notNumeric'));
        $form->addText('image', $this->translator->translate('messages.administration.additem.image'))->setRequired($this->translator->translate('messages.administration.additem.imageRequired'));

        $form->addSubmit('submit', $this->translator->translate('messages.administration.additem.additemButton'));

        $form->onSuccess[] = $this->addItemFormSucceeded;
        return $form;
    }

    public function addItemFormSucceeded(Form $form, ArrayHash $values){
        $item = new Item;
        $item->name = $values->name;
        $item->price = $values->price;
        $item->discount = $values->discount;
        $item->newid = $values->newid;
        $item->itemdata = null;
        $item->image = $values->image;
        $item->visible = 1;
        $item->oldid = null;
        $this->entMng->persist($item);
        $this->entMng->flush();

        $this->flashMessage($this->translator->translate('messages.administration.additem.itemAdded', ['name' => $values->name]), "danger");
        $this->redirect("this");
    }

    public function actionCarts($userId = null){
        if($userId == null){
            $this->flashMessage($this->translator->translate('messages.administration.cart.chooseUser'), "info");
            $this->redirect("users");
        }
    }
}