$(function(){
    $.nette.init();
});
$(document).ready(function(){
    $(document).tooltip();
});

function calculatePrice(priceForEA, ea, ID, mena, discount) {
    var text = "";
    if(discount !== 0)
        text += "<s>"+ priceForEA * ea + mena +"</s> ";
    text += (priceForEA - ((discount/100) * priceForEA)) * ea + mena;
    $('span#endPrice_' + ID).html(text);
    $('a.cart#endPrice_' + ID).attr('href', "?do=addToCart&itemId="+ ID +"&itemCount="+ ea);
    $('a.fast#endPrice_' + ID).attr('href', "?do=fastToServer&itemId="+ ID + "&itemCount="+ea);
}

var move;
function userInfoMove() {
    if($(window).width() <= 980){
        if(move !== true){
            $('.user-info').css('top', '-20px');
            $('.user-info-arrow').css('top', '-40px');
            $('.user-info-arrow>img').css("transform", "rotate(180deg)");
            move = true;
        } else{
            $('.user-info').css('top', '-180px');
            $('.user-info-arrow').css('top', '-200px');
            $('.user-info-arrow>img').css("transform", "rotate(0deg)");
            move = false;
        }
    } else{
        if(move !== true) {
            $('.user-info').animate({
                top: "-20px"
            }, 250);
            $('.user-info-arrow').animate({
                top: "-40px"
            }, 250);
            $('.user-info-arrow>img').css("transform", "rotate(180deg)");
            move = true;
        } else {
            $('.user-info').animate({
                top: "-180px"
            }, 250);
            $('.user-info-arrow').animate({
                top: "-200px"
            }, 250);
            $('.user-info-arrow>img').css("transform", "rotate(0deg)");
            move = false;
        }
    }
}

$.nette.ext({
    before: function(xrh, settings){
        if (!settings.nette) return;

        var question = settings.nette.el.data("confirm");
        if (question) {
            return confirm(question);
        }
    },
    success: function(payload){
        alertify.logPosition("bottom right");
        if(typeof payload.successMessage !== 'undefined') {
            alertify.success(payload.successMessage);
        }

        if(typeof payload.errorMessage !== 'undefined'){
            alertify.error(payload.errorMessage);
        }
    }
});

var typingTimeout;
$('.search-ajax').keyup(function (){
    if(typingTimeout)
        clearTimeout(typingTimeout);

    typingTimeout = setTimeout(sendSearchRequest, 1000);
});

function sendSearchRequest(){
    var value = $('.search-ajax').val();

    if(value === ""){
        $('.pagination').show(500);
    }else{
        $('.pagination').hide(500);
    }

    $.nette.ajax({
        'url': '?do=search',
        'data': {
            'q': value
        }
    });
}