<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="ws_cart")
 */
class CartItem
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @var User
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @var Item
     */
    protected $item;

    /**
     * @ORM\Column(type="integer")
     */
    protected $count;
    
}