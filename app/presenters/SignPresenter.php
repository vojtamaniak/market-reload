<?php
namespace App\Presenters;

use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Utils\ArrayHash;

class SignPresenter extends BasePresenter
{
    protected function createComponentSignInForm(){
        $form = new Form;

        $form->addText("username")->setRequired($this->translator->translate("messages.signin.nonick"))->setAttribute("placeholder", $this->translator->translate("messages.signin.nick"));

        $form->addPassword("password")->setRequired($this->translator->translate("messages.signin.nopassword"))->setAttribute("placeholder", $this->translator->translate("messages.signin.password"));

        $form->addCheckbox("remember");

        $form->addSubmit("submit", $this->translator->translate("messages.signin.signinButton"));

        $form->onSuccess[] = $this->signInFormSucceeded;
        return $form;
    }

    public function signInFormSucceeded(Form $form, ArrayHash $values)
    {
        try
        {
            $this->getUser()->login($values->username, $values->password);
            $this->flashMessage($this->translator->translate("messages.signin.successfull"), "success");
            $this->redirect("Homepage:");
        }
        catch(AuthenticationException $e)
        {
            $form->addError($this->translator->translate("messages.signin.incorrect"));
        }
    }

    public function actionOut(){
        $this->getUser()->logout();
        $this->flashMessage($this->translator->translate("messages.signout.successfull"), "success");
        $this->redirect("in");
    }
}