<?php

namespace App\Model;

use App\AuthmeUser;
use App\User;
use Kdyby\Doctrine\EntityManager;
use Nette;


/**
 * Users management.
 */
class UserManager extends Nette\Object implements Nette\Security\IAuthenticator
{

    /**
     * @var EntityManager
     */
    private $entityManager;

	/**
	 * @var string
	 */
	private $authmeHash;

	public function __construct(EntityManager $entityManager)
	{
        $this->entityManager = $entityManager;
	}

	public function setAuthmeHash($hash){
		$this->authmeHash = $hash;
	}

	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;

		$user = $this->entityManager->getRepository(AuthmeUser::class)->findOneBy(array('username' => $username));

		if($user == null){
            throw new Nette\Security\AuthenticationException("Unknown nickname.", self::IDENTITY_NOT_FOUND);
        }

        $truepass = $user->password;
        switch ($this->authmeHash) {
            case 'MD5':
                $encryptpass = hash("MD5", $password);
                break;
            case 'SHA1':
                $encryptpass = hash("SHA1", $password);
                break;
            case 'SHA256':
                $salt = explode('$', $truepass);
                $encryptpass = '$SHA$' . $salt[2] . '$' . hash("sha256", hash("sha256", $password) . $salt[2]);
                break;
            default:
                throw new \Exception("Unsupported Authme hash.");
        }

        if ($encryptpass !== $truepass) {
            throw new Nette\Security\AuthenticationException("Wrong password.", self::INVALID_CREDENTIAL);
        }

        $wsUser = $this->entityManager->getRepository(User::class)->findOneBy(array("username" => $user->username));
        if($wsUser == null){
            $wsUser = new User;
            $wsUser->username = $user->username;
            $wsUser->role = "default";
            $wsUser->lastlogin = new \DateTime();
            $this->entityManager->persist($wsUser);
        }else{
            $wsUser->lastlogin = new \DateTime();
        }
        $this->entityManager->flush();

		return new Nette\Security\Identity($wsUser->getId(), $wsUser->role, array('username' => $user->username));
	}

}
