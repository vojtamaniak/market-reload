<?php

namespace App;

use Nette\Object;

class Params extends Object
{

    /**
     * @var int
     * 0 = fe
     */
    private $economyPlugin;


    public function __construct($economyPlugin)
    {
        switch(strtolower($economyPlugin)){
            case "fe":
            case "feconomy":
            case "feeconomy":
                $this->economyPlugin = 0;
                break;
            default:
                throw new \Exception("Unknown economy plugin.");
        }
    }

    /**
     * @return int
     */
    public function getEconomyPlugin()
    {
        return $this->economyPlugin;
    }

}